package com.ajirayaan.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoverConfiguration {

	private List<Scenario> scenarios;
	private List<State> states;
	@JsonProperty("deploy-point")
	private DeployPoint deployPoint;
	@JsonProperty("initial-battery")
	private Integer initialBattery;
	private List<InventoryItem> inventory;

}
