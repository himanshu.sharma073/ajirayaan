package com.ajirayaan.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum SampleType {
	@JsonProperty("water-sample")
	water_sample, 
	@JsonProperty("rock-sample")
	rock_sample;
}
