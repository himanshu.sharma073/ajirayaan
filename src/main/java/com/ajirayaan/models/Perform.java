package com.ajirayaan.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Perform {
	
	@JsonProperty("collect-sample")
	private CollectSample collectSample;
	@JsonProperty("item-usage")
	private ItemUsage itemUsage;
}
