package com.ajirayaan.models;

public class Messages {
	
	public static final String AOB = "Can move only within mapped area";
	public static final String STORM = "Cannot move during a storm";

}
