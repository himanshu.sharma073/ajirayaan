package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoverStatusResponse {
	private RoverStatus rover;
	private EnvironmentStatus environment;
}
