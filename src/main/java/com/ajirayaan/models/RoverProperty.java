package com.ajirayaan.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum RoverProperty {
	battery, terrain, temperature, humidity, @JsonProperty("solar-flare")
	solar_flare, storm;
}
