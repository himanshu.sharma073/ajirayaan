package com.ajirayaan.models;

public enum Operator {
	eq, ne, lte, gte, lt, gt;
}
