package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CollectSample {
	private SampleType type;
	private Integer qty;
}
