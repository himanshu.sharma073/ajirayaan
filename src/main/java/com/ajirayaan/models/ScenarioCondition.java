package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ScenarioCondition {

	private String name;
	private RoverProperty property;
	private Operator operator;
	private Object value;
}
