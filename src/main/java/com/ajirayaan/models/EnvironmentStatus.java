package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EnvironmentStatus extends EnvironmentConfiguration {
	private TerrainType terrain;
}
