package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InventoryItem {
	
	private InventoryItemType type;
	private Integer qty;
	private Integer priority;
}
