package com.ajirayaan.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class State {
	
	private String name;
	@JsonProperty("allowed-actions")
	private List<Action> allowedActions;
}
