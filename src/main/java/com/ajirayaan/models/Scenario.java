package com.ajirayaan.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Scenario {
	
	private String name;
	private List<ScenarioCondition> conditions;
	private List<Rover> rover;

}
