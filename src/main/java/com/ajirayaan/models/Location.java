package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Location {
	private Integer row;
	private Integer column;
}
