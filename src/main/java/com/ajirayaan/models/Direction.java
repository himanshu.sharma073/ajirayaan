package com.ajirayaan.models;

public enum Direction {
	up, down, left, right;
}
