package com.ajirayaan.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EnvironmentConfiguration {

	private Integer temperature;
	private Integer humidity;
	@JsonProperty("solar-flare")
	private Boolean solarFlare;
	private Boolean storm;

}
