package com.ajirayaan.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum InventoryItemType {

	@JsonProperty("storm-shield")
	storm_shield,
	@JsonProperty("water-sample")
	water_sample, 
	@JsonProperty("rock-sample")
	rock_sample;
}
