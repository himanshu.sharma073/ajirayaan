package com.ajirayaan.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EnvironmentConfigurationRequest {

	private Integer temperature;
	private Integer humidity;
	@JsonProperty("solar-flare")
	private Boolean solarFlare;
	private Boolean storm;
	@JsonProperty("area-map")
	private List<List<TerrainType>> areaMap;

}
