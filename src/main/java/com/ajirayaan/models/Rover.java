package com.ajirayaan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Rover {
	private String is;
	private Perform performs;
}
