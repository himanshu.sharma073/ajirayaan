package com.ajirayaan.models;

public enum TerrainType {
	dirt, water, rock, sand;
}
