package com.ajirayaan.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Action {
	move, @JsonProperty("collect-sample")collect_sample;
}
