package com.ajirayaan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajirayaan.models.EnvironmentConfigurationRequest;
import com.ajirayaan.services.RoverService;

@RestController
@RequestMapping("/api/environment")
public class EnvironmentController {
	
	@Autowired
	private RoverService roverService;
	
	@PostMapping("/configure")
	public void configure(@RequestBody EnvironmentConfigurationRequest request) {
		roverService.configure(request);
	}

	@PatchMapping("/")
	public void modify(@RequestBody EnvironmentConfigurationRequest request) {
		roverService.modify(request);
	}
	
}
