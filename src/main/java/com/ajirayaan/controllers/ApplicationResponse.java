package com.ajirayaan.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApplicationResponse {
	
	public String message;

}
