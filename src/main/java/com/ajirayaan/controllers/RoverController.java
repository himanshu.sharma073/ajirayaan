package com.ajirayaan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajirayaan.models.MoveCommand;
import com.ajirayaan.models.RoverConfiguration;
import com.ajirayaan.models.RoverStatusResponse;
import com.ajirayaan.services.RoverService;

@RestController
@RequestMapping("/api/rover")
public class RoverController {

	@Autowired
	private RoverService roverService;

	@PostMapping("/configure")
	public void configure(@RequestBody RoverConfiguration request) {
		roverService.configure(request);
	}

	@PostMapping("/move")
	public ResponseEntity<ApplicationResponse> move(@RequestBody MoveCommand command) {
		String msg = roverService.move(command);
		if (msg != null && msg.length() > 0) {
			ApplicationResponse resp = new ApplicationResponse();
			resp.setMessage(msg);
			return new ResponseEntity<ApplicationResponse>(resp, HttpStatus.PRECONDITION_REQUIRED);
		}
		return new ResponseEntity<ApplicationResponse>(HttpStatus.OK);
	}

	@GetMapping("/status")
	public RoverStatusResponse status() {
		return roverService.status();
	}

}
