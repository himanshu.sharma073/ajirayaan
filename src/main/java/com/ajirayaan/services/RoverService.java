package com.ajirayaan.services;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.ajirayaan.models.EnvironmentConfiguration;
import com.ajirayaan.models.EnvironmentConfigurationRequest;
import com.ajirayaan.models.EnvironmentStatus;
import com.ajirayaan.models.InventoryItem;
import com.ajirayaan.models.InventoryItemType;
import com.ajirayaan.models.Location;
import com.ajirayaan.models.Messages;
import com.ajirayaan.models.MoveCommand;
import com.ajirayaan.models.Rover;
import com.ajirayaan.models.RoverConfiguration;
import com.ajirayaan.models.RoverStatus;
import com.ajirayaan.models.RoverStatusResponse;
import com.ajirayaan.models.TerrainType;

@Service
public class RoverService {

	private static EnvironmentConfiguration environmentConfig;

	private static RoverConfiguration roverConfig;

	private static RoverStatus roverStatus;

	private static List<List<TerrainType>> areaMap;

	public void configure(EnvironmentConfigurationRequest initialConfig) {
		environmentConfig = new EnvironmentConfiguration();
		BeanUtils.copyProperties(initialConfig, environmentConfig);
		areaMap = initialConfig.getAreaMap();
	}

	public void modify(EnvironmentConfigurationRequest update) {
		if (update.getTemperature() != null) {
			environmentConfig.setTemperature(update.getTemperature());
		}
		if (update.getHumidity() != null) {
			environmentConfig.setHumidity(update.getHumidity());
		}
		if (update.getSolarFlare() != null) {
			environmentConfig.setSolarFlare(update.getSolarFlare());
			if (environmentConfig.getSolarFlare()) {
				roverStatus.setBattery(roverConfig.getInitialBattery());
			}
		}
		if (update.getStorm() != null) {
			environmentConfig.setStorm(update.getStorm());
			if (environmentConfig.getStorm()) {
				InventoryItem shield = null;
				for (InventoryItem item : roverStatus.getInventory()) {
					if (item.getType() == InventoryItemType.storm_shield) {
						shield = item;
						break;
					}
				}
				if (shield.getQty() == 0) {
					System.exit(0);
				} else if(shield.getQty() == 1) {
					roverStatus.getInventory().remove(shield);
				}
				shield.setQty(shield.getQty() - 1);
			}
		}
		if (update.getAreaMap() != null) {
			areaMap = update.getAreaMap();
		}
	}

	public RoverStatusResponse status() {
		RoverStatusResponse response = new RoverStatusResponse();
		response.setRover(roverStatus);
		EnvironmentStatus status = new EnvironmentStatus();
		BeanUtils.copyProperties(environmentConfig, status);
		status.setTerrain(areaMap.get(roverStatus.getLocation().getRow()).get(roverStatus.getLocation().getColumn()));
		response.setEnvironment(status);
		return response;
	}

	public void configure(RoverConfiguration request) {
		roverConfig = request;
		roverStatus = new RoverStatus();
		roverStatus.setBattery(request.getInitialBattery());
		roverStatus.setInventory(request.getInventory());
		Location location = new Location();
		location.setRow(request.getDeployPoint().getRow());
		location.setColumn(request.getDeployPoint().getColumn());
		roverStatus.setLocation(location);
	}

	public String move(MoveCommand command) {
		if (environmentConfig.getStorm()) {
			return Messages.STORM;
		}
		switch (command.getDirection()) {
		case up:
			if (roverStatus.getLocation().getRow() == 0)
				return Messages.AOB;
			roverStatus.getLocation().setRow(roverStatus.getLocation().getRow() - 1);
			break;
		case down:
			if (roverStatus.getLocation().getRow() == areaMap.size() - 1)
				return Messages.AOB;
			roverStatus.getLocation().setRow(roverStatus.getLocation().getRow() + 1);
			break;
		case left:
			if (roverStatus.getLocation().getColumn() == 0)
				return Messages.AOB;
			roverStatus.getLocation().setColumn(roverStatus.getLocation().getColumn() - 1);
			break;
		case right:
			if (roverStatus.getLocation().getColumn() == areaMap.get(0).size())
				return Messages.AOB;
			roverStatus.getLocation().setColumn(roverStatus.getLocation().getColumn() + 1);
			break;
		}
		roverStatus.setBattery(roverStatus.getBattery() - 1);
		return null;
	}

}
