# Ajirayaan

Ajira Hackathon Challenge.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Building Application
You can use the below command to build the application:

```shell
mvn clean install
```

## Running the application locally

There are several ways to run this application on your local machine. One way is to execute the `main` method in the `com.ajirayaan.Ajirayaan` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
